#!/bin/bash

set -e
set -o pipefail
set -o verbose
set -o xtrace
export SHELLOPTS

make showvars compile daikon.jar
curl -s -L https://bit.ly/3eH2xMr | bash
make dyncomp-jdk
make -C tests MPARG=-j1 nonquick-txt-diff results
